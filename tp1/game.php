<?php

function initGrid($size){
    $grid = [];
    for ($x = 0; $x < $size[0]; $x++){
        for($y = 0; $y < $size[1]; $y++){
            if(rand(0,1) == 0){
                $grid[$x][$y]= ' ';
            }else {
                $grid[$x][$y]= 'O';
            }
        }
    }
    return $grid;
}

function printGrid($grid, $size) {
    echo '<table>';
    for($x = 0; $x < $size[0]; $x++) {
        echo "<tr>";
            for($y = 0; $y < $size[1]; $y++) {
                echo "<td>" . $grid[$x][$y] . "</td>"; 
            }
        echo "</tr>";
    }
    echo '</table>';
}


function nbNear($grid,$size,$case) {
        
    $x = $case[0];
    $y = $case[1];
    
    $near = 0;

    if($x != 0 && $y != 0) {
        if($grid[$x-1][$y-1] == 'O') {
            $near++;
        }    
    }
    if($x != 0 && $y != $size[1]-1) {
        if($grid[$x-1][$y+1] == 'O') {
            $near++;  
        }  
    }
    if($x != $size[0]-1 && $y != 0) {
        if($grid[$x+1][$y-1] == 'O') {
            $near++;
        }
    }
    if($x != $size[0]-1 && $y != $size[1]-1) {
        if($grid[$x+1][$y+1] == 'O') {
            $near++;  
        }
    }
    if($x != 0) {
        if($grid[$x-1][$y] == 'O') {
            $near++;  
        }
    }
    if($y != 0) {
        if($grid[$x][$y-1] == 'O') {
            $near++;
        }
    }
    if($y != $size[1]-1) {
        if($grid[$x][$y+1] == 'O') {
            $near++;  
        }
    }
    if($x != $size[0]-1) {
        if($grid[$x+1][$y] == 'O') {
            $near++; 
        }
    }
    
    return $near;
}

function change($grid,$size,$case){
    $x = $case[0];
    $y = $case[1];
    
    $near = nbNear($grid,$size,$case);
    
    if($grid[$x][$y] == 'O' && ($near < 2 || $near >= 4)){
        return ' ';
    }elseif ($grid[$x][$y] == ' ' && $near == 3) {
        return 'O';
    }else {
        return $grid[$x][$y];
    }
}

function nextGrid($grid,$grid2,$size){
    for ($x = 0; $x < $size[0]; $x++){
        for ($y = 0; $y < $size[1]; $y++) {
            $case[0] = $x;
            $case[1] = $y;
            $grid2[$x][$y] = change($grid, $size, $case);
        }
    }
    return $grid2;
}
