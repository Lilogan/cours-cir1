<?php
session_start();
include 'grid_template.html';
include 'game.php';

$size = [20,40];

if(isset($_GET['reset'])){
   unset($_SESSION['grid']);
}

if(!isset($_SESSION['grid'])){
    $grid = initGrid($size);
    printGrid($grid, $size);
    $_SESSION['grid'] = $grid;
} else {
    $grid = $_SESSION['grid'];
    printGrid($grid, $size);
    $grid2 = nextGrid($grid, $grid2, $size);
    $_SESSION['grid'] = $grid2;
 
} 
$grid = $grid2;