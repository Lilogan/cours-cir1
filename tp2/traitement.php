<?php
define("TARGET_DIRECTORY", "./upload/");

if(ctype_alnum($_POST["name"])){
  $name = $_POST["name"];
}

if(is_numeric($_POST["price"]) && $_POST["price"] >= 0){
  $price = number_format($_POST["price"], 2);
}

if(is_numeric($_POST["price"]) && $_POST["qty"] >= 0){
  $qty = number_format($_POST["qty"]);
    }

if(!empty($_FILES["pic"])) {
  $cheminPic = TARGET_DIRECTORY . $_FILES["pic"]["name"];
  move_uploaded_file($_FILES["pic"]["tmp_name"], $cheminPic);
}

if(isset($name) && isset($price) && isset($qty) && isset($cheminPic)){
  $file = fopen("mesproduts.csv", "a+");
  fwrite($file,"$name;$price;$qty;$cheminPic\n");
  fclose($file);
  echo "<script>alert('Produit ajoute avec succes'); window.location= 'ajout_produits.php';</script>";
}else {
  echo "<script>alert('Erreur dans le format des entrees'); window.location= 'ajout_produits.php';</script>";
}
