<?php
?>
<html>
    <body>
        <h1>Ajout d'un nouvel article</h1>
        <form action="./traitement.php" method="POST" enctype="multipart/form-data">
            <fieldset id="item">
                <p>
                    <label for="name">Nom du produit</label><input type="text" name="name" id="name"/>
                </p>
                <p>
                    <label for="qty">Quantité</label><input type="number" name="qty" min="0" id="qty"/>
                </p>
                <p>
                    <label for="price">Prix</label><input type="number" step="0.01" name="price" min="0" id="price">
                </p>
                <p>
                    <label for="pic">Photo</label><input type="file" name="pic" id="pic">
            </fieldset>
            <p>
                <input type="submit" value="Envoyer"/>
            </p>
        </form>
    </body>
</html>
