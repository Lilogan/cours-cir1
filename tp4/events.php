<?php
require 'head.html';
require 'function.php';
session_start();

$db = connectDB();
$rank = $_SESSION["rank"];

echo"<div class='row'>";
back();
disconnect();
echo"</div>";

if(isset($_GET['id'])){
    $_SESSION['event_id'] = filter_input(INPUT_GET,"id",FILTER_SANITIZE_NUMBER_INT);
}

if(strcmp($rank,"CUSTOMER") == 0){
    if(isset($_GET['register']) && !userParticipate($db, $_SESSION['user_id'], $_SESSION['event_id'])){
        addUserEvent($db, $_SESSION['user_id'], $_SESSION['event_id']);
        echo "<div class='row justify-content-center' style='color: turquoise'>You are register to the event.</div>";
    }
    if(isset($_GET['leave']) && userParticipate($db, $_SESSION['user_id'], $_SESSION['event_id'])){
        removeUserEvent($db, $_SESSION['user_id'], $_SESSION['event_id']);
        echo "<div class='row justify-content-center' style='color: red'>You have leave the event.</div>";
    }
}
if(strcmp($rank,"ORGANIZER") == 0){
    if(isset($_GET['delete'])){
        deleteEvent($db, $_SESSION['event_id']);
        $_SESSION['eventSuccess'] = "Event was delete";
        redirect();
    }
}

$event = getEventById($db, $_SESSION['event_id']);
$start = date("l, F jS Y",strtotime($event['startdate']));
$end = date("l, F jS Y",strtotime($event['enddate']));
$place = $event['nb_place'];
$participant = numberUserEvent($db, $_SESSION['event_id']);
?>

<div class="container">
    <h1 style="text-align: center"><?php echo htmlspecialchars($event['name']);?></h1>
    <div class="row">From <?php echo $start;?> to <?php echo $end;?>. </div>
    <div class="row">Description : <?php echo htmlspecialchars($event['description']);?></div>
    <div class="row">Participant : <?php echo $participant;?>/<?php echo $place;?></div>
    <?php
    if(strcmp($rank,"CUSTOMER") == 0) {
        if(userParticipate($db, $_SESSION['user_id'], $_SESSION['event_id'])){
            echo "<div class='row'><a class='btn btn-danger btn-block' role='button' href=?leave >Leave</a></div>";
        }else{
            echo "<div class='row'><a class='btn btn-primary btn-block' role='button' href=?register >Register</a></div>";
        }
    }
    if(strcmp($rank,"ORGANIZER") == 0){
        echo "<div class='row'><a class='btn btn-danger btn-block' role='button' href=?delete >Delete</a></div>";
    }
    ?>
</div>


