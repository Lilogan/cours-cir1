<?php
session_start();

require('function.php');

$db = connectDB();

require('head.html');

if(isset($_GET['back'])){
    unset($_SESSION['event_id']);
}

if(isset($_SESSION['rank'])) {
    if(isset($_GET['logout'])){
        session_destroy();
        redirect();
    }
    $rank = $_SESSION['rank'];
    if($rank == 'ORGANIZER' || $rank == 'CUSTOMER'){
        require('calendar.php');
    }
}else{
    if(isset($_SESSION['connectError'])){
        $error = $_SESSION['connectError'];
        echo "<p style='text-align: center; color: red'>".$error."</p>";
        unset($_SESSION['connectError']);
    }
    require('form_login.html');
}
