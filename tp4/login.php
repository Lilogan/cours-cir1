<?php
session_start();

include('function.php');

if(isset($_POST['user'])){
    $user = filter_input(INPUT_POST,"user",FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}
if(isset($_POST['password'])){
    $password = filter_input(INPUT_POST,"password",FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}

$db = connectDB();
$req = $db->prepare('SELECT * FROM Users WHERE login = :login');

$req->execute(array(':login' => $user));

if($req->rowCount() < 1) {
    $_SESSION['connectError'] = "Incorrect Username";
    redirect();
}

$userData = $req->fetch();

if(password_verify($password, $userData['password'])) {
    $_SESSION['rank'] = $userData['rank'];
    $_SESSION['user_id'] = $userData['id'];
}else{
    $_SESSION['connectError'] = "Incorrect Password";
}
redirect();