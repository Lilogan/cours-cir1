<?php
session_start();
require 'function.php';

$db = connectDB();

if (!empty($_POST['name'])){
    $name = filter_input(INPUT_POST,'name',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}else{
    $_SESSION['eventError'] = "Incorrect Name";
    redirect();
}

if (!empty($_POST['description'])){
    $description = filter_input(INPUT_POST,'description',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}else{
    $_SESSION['eventError'] = "No Description";
    redirect();
}

if (!empty($_POST['start'])){
    if(check_date($_POST['start'])){
        $start = filter_input(INPUT_POST,'start');
    }else{
        $_SESSION['eventError'] = "Incorrect Starting Date";
    }
}else{
    $_SESSION['eventError'] = "No Starting Date";
    redirect();
}

if (!empty($_POST['end'])){
    if(check_date($_POST['end'])){
        $end = filter_input(INPUT_POST,'end');
    }else{
        $_SESSION['eventError'] = "Incorrect Ending Date";
    }
}elseif (!empty($_POST['duration'])){
    if(is_numeric($_POST['duration'])) {
        $end = date("Y-m-d", strtotime($start . " +" . filter_input(INPUT_POST, 'duration')
            . "day - 1 day"));
    }else{
        $_SESSION['eventError'] = "Incorrect Duration";
        redirect();
    }
}else{
    $_SESSION['eventError'] = "No Ending Date or Duration";
    redirect();
}

if (!empty($_POST['place'])){
    if(is_numeric($_POST['place'])){
        $place = filter_input(INPUT_POST,'place');
    }else{
        $_SESSION['eventError'] = "Incorrect Number of Place";
        redirect();
    }
}else{
    $_SESSION['eventError'] = "No Number of Place";
    redirect();
}

addEvent($db,$name,$description,$start,$end,$_SESSION['user_id'],$place);
$_SESSION['eventSuccess'] = 'Event add with success';

redirect();