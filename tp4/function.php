<?php

function redirect($page = './') {
    header('location: ' . $page);
    exit();
}

function connectDB(){
    try {
        $db = new PDO('mysql:host=localhost;dbname=event_calendar;charset=utf8','lilian','');
    } catch(Exception $e){
        exit('Erreur de connexion à la base de données.');
    }
    return $db;
}

function disconnect(){
    echo"<div style='text-align: right' class='col'><a class='btn btn-primary' role='button' href=./?logout>Logout</a></div>";
}

function back(){
    echo"<div style='text-align: left' class='col'><a class='btn btn-primary' href=./?back role='button'>Return</a></div>";
}

function check_date($x) {
    return (date('Y-m-d', strtotime($x)) == $x);
}

function getEventById(PDO $db, $id){
    $req = $db->prepare('SELECT * FROM events WHERE id = :id');
    $req->execute(array(':id' => $id));

    return $req->fetch();
}

function getEventByDate(PDO $db, $date, $limit = NULL, $organizer_id = NULL){
    $select = 'SELECT * FROM events WHERE :date BETWEEN DATE(startdate) and DATE(enddate)';
    if($organizer_id != NULL) {
        $select .= ' AND organizer_id = :organizer_id';
    }
    if($limit != NULL) {
        $select .= ' LIMIT ' . $limit;
    }
    $arg = [':date' => $date];
    if($organizer_id != NULL) {
        $arg[':organizer_id'] = $organizer_id;
    }
    $req = $db->prepare($select);
    $req->execute($arg);

    return $req->fetchAll();
}

function addEvent(PDO $db, $name, $description, $startdate, $enddate, $organizer_id, $nb_place) {
    $req = $db->prepare('INSERT INTO events (name, description, startdate, enddate, organizer_id,
 nb_place) VALUE (:name, :description, :startdate, :enddate, :organizer_id, :nb_place)');

    $req->execute(array(':name' => $name, ':description' => $description, ':startdate' => $startdate,
        ':enddate' => $enddate, ':organizer_id' => $organizer_id, ':nb_place' => $nb_place
    ));
}

function deleteEvent(PDO $db, $event_id) {
    $reqA = $db->prepare('DELETE FROM user_participates_events WHERE id_event = :id_event');
    $reqB = $db->prepare('DELETE FROM events WHERE id = :id');
    $reqA->execute(array(':id_event' => $event_id));
    $reqB->execute(array(':id' => $event_id));
}

function addUserEvent(PDO $db, $user_id, $event_id) {
    $req = $db->prepare('INSERT INTO user_participates_events SET id_event = :id_event, id_participant = :id_user');
    return $req->execute(array(':id_event' => $event_id, ':id_user' => $user_id));
}

function removeUserEvent(PDO $db, $user_id, $event_id) {
    $req = $db->prepare('DELETE FROM user_participates_events WHERE id_event = :id_event AND id_participant = :id_user');
    return $req->execute(array(':id_event' => $event_id, ':id_user' => $user_id));
}

function numberUserEvent(PDO $db, $event_id) {
    $req = $db->prepare('SELECT * FROM user_participates_events WHERE id_event = :id_event');
    $req->execute(array(':id_event' => $event_id));

    return $req->rowCount();
}

function userParticipate(PDO $db, $user_id, $event_id){
    $req = $db->prepare('SELECT * FROM user_participates_events WHERE id_event = :id_event AND id_participant = :id_user');
    $req->execute(array(':id_event' => $event_id, ':id_user' => $user_id));

    return (bool)$req->rowCount();
}

function eventFull(PDO $db, $event) {
    return (numberUserEvent($db, $event['id']) == $event['nb_place']);
}

function numberEvent(PDO $db, $date){
    $req = $db->prepare('SELECT * FROM events WHERE :date BETWEEN DATE(startdate) AND DATE(enddate)');
    $req->execute(array(':date' => $date));

    return $req->rowCount();
}









