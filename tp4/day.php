<?php
require 'head.html';
require 'function.php';
session_start();

$db = connectDB();
$rank = $_SESSION["rank"];

echo"<div class='row'>";
back();
disconnect();
echo"</div>";

$year = filter_input(INPUT_GET,"year",FILTER_SANITIZE_NUMBER_INT);
$month = filter_input(INPUT_GET,'month',FILTER_SANITIZE_NUMBER_INT);
$day = filter_input(INPUT_GET,"day",FILTER_SANITIZE_NUMBER_INT);
$date = date("Y-m-j",strtotime($year."-".$month."-".$day));
$dateAff = date("l, F jS Y",strtotime($year."-".$month."-".$day));

if($_SESSION['rank'] == 'CUSTOMER') {
    $events = getEventByDate($db, $date);
}elseif($_SESSION['rank'] == 'ORGANIZER'){
    $events = getEventByDate($db, $date,NULL,$_SESSION['user_id']);
}
?>

<div class="container">
    <h1 style="text-align: center">Events of <?php echo $dateAff;?></h1>
<?php
foreach ($events as $event){
    if ($_SESSION['rank'] == 'ORGANIZER' || !eventFull($db,$event) || userParticipate($db,$_SESSION['user_id'],$event['id'])){
        echo "<div class='row justify-content-center'><a href=events.php?id=".$event['id'].">"
            .$event['name']."</a></div>";
    }
}?>
</div>
