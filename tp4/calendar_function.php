<?php

function eventUser($year,$month,$day){
    $date = date('Y-m-d',strtotime($year."-".$month."-".$day));
    $db = connectDB();
    if($_SESSION['rank'] == 'CUSTOMER') {
        $events = getEventByDate($db, $date, 5);
    }elseif($_SESSION['rank'] == 'ORGANIZER'){
        $events = getEventByDate($db, $date, 5,$_SESSION['user_id']);
    }
    foreach ($events as $event){
        echo "<div class='row justify-content-center'><a href=events.php?id=".$event['id'].">"
            .$event['name']."</a></div>";
    }
    if(numberEvent($db,$date) > 5){
        echo "<div class='row justify-content-center'><a class='btn btn-link  btn-sm' role='button'
href=day.php?year=".$year."&month=".$month."&day=" .$day.">See more</a></div>";
    }

}

function calendarCase($year,$month,$day){
    echo "<td>";
    echo "<div class='row justify-content-center'>".$day."</div>";
    eventUser($year,$month,$day);
    echo "</td>";
}


function calendar($month,$year){
    $nbDays = cal_days_in_month(CAL_GREGORIAN,$month,$year);
    echo "<table class='table table-bordered'>";
    $week = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
    echo"<tr>";
    foreach($week as $name){
        echo"<th>".$name."</th>";
    }
    echo "</tr>";
    for($day = 1; $nbDays >= $day; $day++){
        $jd = cal_to_jd(CAL_GREGORIAN, $month, $day, $year);
        $dayWeek = jddayofweek($jd);
        if($day == $nbDays){
            if($dayWeek == 1){
                echo "<tr>";
            }
            calendarCase($year,$month,$day);

            if($dayWeek != 0){
                while($dayWeek < 7){
                    echo "<td></td>";
                    $dayWeek++;
                }
            }
            echo"</tr>";
        }
        else if ($day == 1){
            echo "<tr>";
            if($dayWeek == 0){
                $dayWeek = 7;
            }
            for($j = 1; $j != $dayWeek; $j++){
                echo "<td></td>";
            }
            calendarCase($year,$month,$day);
            if($dayWeek == 7){
                echo "</tr>";
            }

        }
        else{
            if($dayWeek == 1){
               echo "<tr>";
            }
            calendarCase($year,$month,$day);
            if($dayWeek == 0){
                echo "</tr>";
            }
        }
    }
    echo "</table>";
}


function nameMonth($nb){
    $name = array("January","February","March","April","May","June","July","August","September","October",
        "November","December");
    return $name[$nb-1];
}
