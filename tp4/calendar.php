<?php
require('calendar_function.php');

disconnect();

if(isset($_GET['month'])){
    $_SESSION['month'] = $_GET['month'];
}else if(!isset($_SESSION['month'])){
    $_SESSION['month'] = date("n");
}

if(isset($_GET['year'])){
    $_SESSION['year'] = $_GET['year'];
}else if(!isset($_SESSION['year'])){
    $_SESSION['year'] = date("Y");
}

if($_SESSION['month'] > 12){
    $_SESSION['month'] -= 12;
    $_SESSION['year']++;
}
if($_SESSION['month'] < 1){
    $_SESSION['month'] += 12;
    $_SESSION['year']--;
}

?>

<h1 style ="text-align: center">
    <a class="btn btn-primary" role="button" href="?month=<?php echo $_SESSION['month']-1?>"> &larr; </a>
    <?php echo htmlspecialchars(nameMonth($_SESSION['month'])."  ".$_SESSION['year']);?>
    <a class="btn btn-primary" role="button" href="?month=<?php echo $_SESSION['month']+1?>"> &rarr; </a>
</h1>

<div style="margin-bottom: 50px" class="container">
    <?php calendar($_SESSION['month'],$_SESSION['year']); ?>
    <a class="btn btn-primary btn-block" role="button" href="?month=<?php echo date("n")?>&year=<?php echo date("Y")?>"> Now </a>
</div>


<?php
if($_SESSION['rank'] == 'ORGANIZER'){
    if(isset($_SESSION['eventError'])) {
        $error = $_SESSION['eventError'];
        echo "<p style='text-align: center; color: red'>".$error."</p>";
        unset($_SESSION['eventError']);
    }elseif(isset($_SESSION['eventSuccess'])) {
        $success = $_SESSION['eventSuccess'];
        echo "<p style='text-align: center; color: turquoise'>".$success."</p>";
        unset($_SESSION['eventSuccess']);
    }

    include "form_event.html";
}
?>


